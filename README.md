# rcAddReponseCode

Allow to use expression manager, expression script question code when update or insert new response.

This plugin was tested from LimeSurvey 3 to 5. alpha version for LimeSurvey 6

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06)
- Clone in plugins/rcAddReponseCode directory

### Via ZIP dowload
- Get the file and uncompress it
- Move the file included to plugins/rcAddReponseCode directory

## LimeSurvey configuration

### With LimeSurvey 2.06lts or lesser
To allow plugin to be called via RPC POST routine, you need to disable CsrfValidation for plugins/direct'. This can be done in application/config/config.php.
````
	'components' => array(
		'db' => array(
			[...]
		),
		'request' => array(
			'class'=>'LSHttpRequest',
			'noCsrfValidationRoutes'=>array(
				'remotecontrol',
				'plugins/direct'
		),
	[...]
	),
````

### With LimeSurvey 2.50 after build 160330 or 2.06_sp_1.0.9

The plugin can be used with newUnsecureRequest event

## Usage

This plugin extend [LimeSurvey RemoteControl 2 API](https://manual.limesurvey.org/RemoteControl_2_API). New function are added to LimeSurvey API with the new url created by the plugin. 

You can call the new function with the link shown in plugin settings. If you publish API in LimeSurvey core config : another url is show where you can see all available function (the new one and the other from LimeSurvey core).

Since authentification and `get_session_key` is the same function, you can create the session key with the core remote control url and use it in the plugin remote control url. This allow different plugin with different function, using same control key.

This plugin was tested only with JSON-RPC.

### Function detail

Add a response to the survey responses collection and returns the id of the inserted survey response

`add_response_code($sSessionKey, $iSurveyID, $aResponseData[, $existingId='ignore'][, $emCode=true])`

- string $sSessionKey Auth credentials 
- int $iSurveyID ID of the Survey to insert responses
- struct $aResponseData The actual response data in an array with key for code or columns name.
- string $existingId if is is set :
    - replace : replace all response by current data
    - replaceanswers : replace existing column, keep other columns (update an answer)
    - renumber : create a new response with another number
    - ignore : don't add response, return an error witgh status set to `Ignore exsiting id`
- boolean $emCode allowed to use EM code, else use only columns name

Return the response id in case of success, array with status in case of error.

The function did not include the feature to move files from upload directory to survey directory.

## Home page, Copyright and support
- HomePage <http://extensions.sondages.pro/extendremotecontrol/>
- Copyright © 2015-2025 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- Issues <https://gitlab.com/SondagesPro/RemoteControl/extendRemoteControl/issues>
- Professional support <https://www.sondages.pro/contact.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro)
